﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Cargold.Observer;
using System;

namespace Cargold.FrameWork
{
    public class UiSystem_Manager : MonoBehaviour, GameSystem_Manager.IInitialize
    {
        [SerializeField] private UI_Toast_Manager uI_Toast_Manager;

        public Toast toast;

        public void Init_Func(int _layer)
        {
            if(_layer == 0)
            {
                this.uI_Toast_Manager.Init_Func();

                this.toast.Init_Func();
            }
        }

        #region Common
        [System.Serializable]
        public class Toast
        {
            private Observer_Action<string> obs;

            public void Init_Func()
            {
                this.obs = new Observer_Action<string>();
            }

            public void Subscribe_Func(Action<string> _del)
            {
                this.obs.Subscribe_Func(_del);
            }

            public void Unsubscribe_Func(Action<string> _del)
            {
                this.obs.Unsubscribe_Func(_del);
            }
        }
        #endregion
    } 
}