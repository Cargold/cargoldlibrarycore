﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Cargold.FrameWork
{
    public class UI_Toast_Manager : SerializedMonoBehaviour
    {
        public static UI_Toast_Manager Instance;

        [ReadOnly, ShowInInspector] private List<UI_Toast_Script> toastElemClassList;

        public virtual void Init_Func()
        {
            Instance = this;

            this.toastElemClassList = new List<UI_Toast_Script>();

            this.Deactivate_Func(true);
        }

        public virtual void Activate_Func(string _toastKey)
        {
            UI_Toast_Script _toastElemClass = UI_Toast_Script.Actiavte_Func(_toastKey);
            this.toastElemClassList.Add(_toastElemClass);
        }

        public void DeactivateElem_Func(UI_Toast_Script _toastElemClass)
        {
            this.toastElemClassList.Remove_Func(_toastElemClass);
        }

        public void Deactivate_Func(bool _isInit = false)
        {
            if (_isInit == false)
            {
                for (int i = toastElemClassList.Count - 1; i >= 0; i--)
                {
                    UI_Toast_Script _toastElemClass = this.toastElemClassList[i];
                    _toastElemClass.Deactivate_Func(true);
                }
            }

            this.toastElemClassList.Clear();
        }

        public virtual string GetLczKey_Func(string _toastKey)
        {
            return default;
        }
    }
}