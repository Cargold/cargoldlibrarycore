﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using PlayHard.PoolingSystem;
using TMPro;

namespace Cargold.FrameWork
{
    public class UI_Toast_Script : MonoBehaviour, IPooler
    {
        [BoxGroup(DataBase_Manager.FrameWork)] [SerializeField] private GameObject groupObj = null;
        [BoxGroup(DataBase_Manager.FrameWork)] [SerializeField] private Animation anim = null;
        [BoxGroup(DataBase_Manager.FrameWork)] [SerializeField] private AnimationClip showClip = null;
        [BoxGroup(DataBase_Manager.FrameWork)] [SerializeField] private TextMeshProUGUI contentTmp = null;

        public void Init_Func()
        {
            RectTransform _rtrf = this.transform as RectTransform;
            _rtrf.sizeDelta = new Vector2(0f, _rtrf.sizeDelta.y);
            _rtrf.anchoredPosition = Vector2.zero;
            _rtrf.localScale = Vector3.one;

            this.Deactivate_Func(false, true);
        }

        private void Activate_Func(string _toastKey)
        {
            this.groupObj.SetActive(true);

            this.transform.SetAsLastSibling();

            this.anim.Play_Func(this.showClip);

            string _lczKey = UI_Toast_Manager.Instance.GetLczKey_Func(_toastKey);
            this.contentTmp.text = LocalizationSystem_Manager.Instance.GetLcz_Func(_lczKey);
        }

        public void Deactivate_Func(bool _isCallManager = false, bool _isInit = false)
        {
            if (_isInit == false)
            {
                if (_isCallManager == false)
                    UI_Toast_Manager.Instance.DeactivateElem_Func(this);

                PoolingSystem_Manager.Instance.Despawn_Func(PoolingKey.ToastElem, this);
            }

            this.groupObj.SetActive(false);
        }

        public void CallAni_Close_Func()
        {
            this.Deactivate_Func();
        }

        void IPooler.InitializedByPoolingSystem()
        {
            this.Init_Func();
        }

        public static UI_Toast_Script Actiavte_Func(string _toastKey)
        {
            UI_Toast_Script _elemClass = PoolingSystem_Manager.Instance.Spawn_Func<UI_Toast_Script>(PoolingKey.ToastElem);
            _elemClass.Activate_Func(_toastKey);

            return _elemClass;
        }
    } 
}