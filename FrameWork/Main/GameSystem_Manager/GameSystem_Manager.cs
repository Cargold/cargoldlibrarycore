﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cargold.FrameWork
{
    public class GameSystem_Manager : SerializedMonoBehaviour
    {
        [BoxGroup(DataBase_Manager.FrameWork), HideLabel, SerializeField] private IInitialize[] iInitArr = new IInitialize[0];

        private void Awake()
        {
            this.Init_Func();
        }

        protected virtual void Init_Func()
        {
            for (int _layer = 0; _layer < 2; _layer++)
            {
                foreach (IInitialize _iInit in this.iInitArr)
                    _iInit.Init_Func(_layer); 
            }

            GameObject.DontDestroyOnLoad(this.gameObject);
        }

        public interface IInitialize
        {
            void Init_Func(int _layer);
        }
    } 
}