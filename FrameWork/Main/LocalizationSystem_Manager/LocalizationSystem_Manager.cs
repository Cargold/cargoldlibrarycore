﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace Cargold.FrameWork
{
    public class LocalizationSystem_Manager : MonoBehaviour, GameSystem_Manager.IInitialize
    {
        private const string FormatMHMS = "{0}{1} {2:00}{3} {4:00}{5} {6:00}{7}";
        private const string DayStr = "D";
        private const string HourStr = "H";
        private const string MinuteStr = "M";
        private const string SecondStr = "S";

        public static LocalizationSystem_Manager Instance;

        [FoldoutGroup(DataBase_Manager.FrameWork), ReadOnly, ShowInInspector] private LanguageType languageType = LanguageType.None;

        public virtual void Init_Func(int _layer)
        {
            if (_layer == 0)
            {
                Instance = this;
            }
            else if (_layer == 1)
            {
                this.languageType = UserSystem_Manager.Instance.common.GetLanguageType_Func();
            }
        }

        public string GetLcz_Func(string _lczID, LanguageType _languageType = LanguageType.None)
        {
            if (_languageType == LanguageType.None)
                _languageType = this.languageType;

            return DataBase_Manager.Instance.GetLocalize.GetLcz_Func(_lczID, _languageType);
        }

        public string GetLcz_Func(DateTime _dateTime, bool _isContainDay = false, LanguageType _languageType = LanguageType.None, string _dayStr = null, string _hourStr = null, string _minStr = null, string _secStr = null)
        {
            string _dayValue = string.Empty;
            _hourStr = string.IsNullOrEmpty(_hourStr) == true ? HourStr : _hourStr;
            _minStr = string.IsNullOrEmpty(_minStr) == true ? MinuteStr : _minStr;
            _secStr = string.IsNullOrEmpty(_secStr) == true ? SecondStr : _secStr;

            int _hour = default;

            if (_isContainDay == false)
            {
                _dayValue = string.Empty;
                _dayStr = string.Empty;

                _hour = (_dateTime.Day - 1) * 24;
            }
            else
            {
                int _day = _dateTime.Day - 1;

                _dayValue = _day.ToString();
                _dayStr = string.IsNullOrEmpty(_dayStr) == true ? DayStr : _dayStr;
            }

            _hour += _dateTime.Hour;
            return string.Format(FormatMHMS, _dayValue, _dayStr, _hour, _hourStr, _dateTime.Minute, _minStr, _dateTime.Second, _secStr);
        }
    }
}

public enum LanguageType
{
    None = 0,

    Korea = 10,
    English = 20,
}