﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Cargold.Observer;
using System;

namespace Cargold.FrameWork
{
    public class UserSystem_Manager : SerializedMonoBehaviour, GameSystem_Manager.IInitialize
    {
        public static UserSystem_Manager Instance;

        [BoxGroup(DataBase_Manager.FrameWork)] [SerializeField] public PH_Common common;

        public virtual void Init_Func(int _layer)
        {
            if (_layer == 0)
            {
                Instance = this;
                
                this.common.Init_Func();
            }
            else if (_layer == 1)
            {

            }
        }

        #region Common
        [System.Serializable]
        public abstract class PH_Common
        {
            protected Observer_Action<LanguageType> langChangedObs;

            public void Init_Func()
            {
                this.langChangedObs = new Observer_Action<LanguageType>();
            }

            public abstract LanguageType GetLanguageType_Func();

            public void SetLanguage_Func(LanguageType _langType)
            {
                // 언어 변경 저장
                this.SaveLanguage_Func(_langType);

                // 언어 변경 이벤트 구독자에게 알림
                this.langChangedObs.Notify_Func(_langType);
            }
            protected abstract void SaveLanguage_Func(LanguageType _langType);

            public void Subscribe_Func(Action<LanguageType> _del)
            {
                this.langChangedObs.Subscribe_Func(_del);
            }

            public void Unsubscribe_Func(Action<LanguageType> _del)
            {
                this.langChangedObs.Unsubscribe_Func(_del);
            }
        }
        #endregion
    } 
}