﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using PlayHard.PoolingSystem;

namespace Cargold.FrameWork
{
    public class PoolingSystem_Manager : MonoBehaviour, GameSystem_Manager.IInitialize
    {
        public static PoolingSystem_Manager Instance;

        private PoolingSystemManager<string> innerPoolManager;

        [BoxGroup("FrameWork")] [SerializeField] private GameObject toastObj = null;

        public virtual void Init_Func(int _layer)
        {
            if(_layer == 0)
            {
                innerPoolManager = new PoolingSystemManager<string>(this.transform);

                this.TryGeneratePool_Func<UI_Toast_Script>(PoolingKey.ToastElem, this.toastObj);
            }
        }

        public T Spawn_Func<T>(string _poolingKey) where T : Component, IPooler
        {
            return this.innerPoolManager.Spawn<T>(_poolingKey);
        }

        public void Despawn_Func<T>(string _poolingKey, T _componentType) where T : Component, IPooler
        {
            this.innerPoolManager.Despawn(_poolingKey, _componentType, true);
        }

        public bool TryGeneratePool_Func<ComponentType>(string _poolingKey, GameObject _poolerObj, Transform _groupTrf = null) where ComponentType : Component, IPooler
        {
            return this.innerPoolManager.TryGeneratePool<ComponentType>(_poolingKey, _poolerObj, 0, true, _groupTrf);
        }
    } 
}

public partial class PoolingKey
{
    public const string ToastElem = "ToastElem";
}