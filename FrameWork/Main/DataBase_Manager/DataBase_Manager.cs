﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Cargold.FrameWork
{
    public abstract class DataBase_Manager : MonoBehaviour, GameSystem_Manager.IInitialize
    {
        public const string FrameWork = "프레임워크";

        public static DataBase_Manager Instance;

        public abstract Localize GetLocalize { get; }

        public virtual void Init_Func(int _layer)
        {
            if (_layer == 0)
            {
                Instance = this;

                this.GetLocalize.Init_Func();
            }
            else if (_layer == 1)
            {

            }
        }

        #region Template
        [System.Serializable]
        public abstract class Template<T> where T : IData
        {
            [FoldoutGroup(DataBase_Manager.FrameWork), ReadOnly, ShowInInspector] private Dictionary<string, T> dataDic;

            public virtual void Init_Func()
            {
                if(this.dataDic == null)
                {
                    this.dataDic = new Dictionary<string, T>();

                    T[] _dataArr = this.GetDataArr_Func();
                    foreach (T _data in _dataArr)
                    {
                        string _key = _data.GetKey;
                        this.dataDic.Add(_key, _data);
                    }
                }
            }

            protected abstract T[] GetDataArr_Func();

            public T GetData_Func(string _key)
            {
                return this.dataDic.GetValue_Func(_key);
            }
        }
        #endregion

        #region Localize
        [System.Serializable]
        public class Localize : Template<LocalizeData>
        {
            [FoldoutGroup(DataBase_Manager.FrameWork), SerializeField] private LocalizeData[] localizeDataArr = null;

            protected override Cargold.FrameWork.LocalizeData[] GetDataArr_Func()
            {
                return this.localizeDataArr;
            }

            public string GetLcz_Func(string _lczID, LanguageType _languageType)
            {
                LocalizeData _lczData = base.GetData_Func(_lczID);

                switch (_languageType)
                {
                    case LanguageType.Korea:    return _lczData.ko;
                    case LanguageType.English:  return _lczData.en;

                    default:
                        Debug_C.Error_Func("_languageType : " + _languageType);
                        return string.Empty;
                }
            }
        }
        #endregion

        public static void DataError_Func(string _key, System.Exception _e)
        {
            Debug_C.Error_Func("DB Load 오류) base.Key : " + _key);
            Debug_C.Warning_Func("_e Data : " + _e.Data);
            Debug_C.Warning_Func("_e Message : " + _e.Message);
            Debug_C.Warning_Func("_e Source : " + _e.Source);
            Debug_C.Warning_Func("_e String : " + _e.ToString());
        }

        public interface IData
        {
            string GetKey { get; }
        }
    }

    #region LocalizeData
    [System.Serializable]
    public class LocalizeData : DataBase_Manager.IData
    {
        [SerializeField] private string key;
        public string ko;
        public string en;

        public string GetKey => this.key;

        public LocalizeData(string _key)
        {
#if UNITY_EDITOR
            try
            {
                Init_Func();
            }
            catch (System.Exception _e)
            {
                DataBase_Manager.DataError_Func(_key, _e);
            }
#else
        Init_Func();
#endif
        }

        protected virtual void Init_Func()
        {

        }
    }
    #endregion
}