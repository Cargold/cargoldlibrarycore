﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DataBase_Manager : Cargold.FrameWork.DataBase_Manager
{
    public static new DataBase_Manager Instance;

    public Localize localize = null;

    public override Cargold.FrameWork.DataBase_Manager.Localize GetLocalize => this.localize;

    public override void Init_Func(int _layer)
    {
        base.Init_Func(_layer);

        if(_layer == 0)
        {
            Instance = this;
        }
    }

    [System.Serializable]
    public new class Localize : Cargold.FrameWork.DataBase_Manager.Localize
    {
        public string testValue;
    }
}
