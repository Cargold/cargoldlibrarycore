﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class GameSystem_Manager : Cargold.FrameWork.GameSystem_Manager
{
    public static GameSystem_Manager Instance;

    protected override void Init_Func()
    {
        base.Init_Func();

        Instance = this;
    }
}
