﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class UserSystem_Manager : Cargold.FrameWork.UserSystem_Manager
{
    public static new UserSystem_Manager Instance;

    public override void Init_Func(int _layer)
    {
        base.Init_Func(_layer);

        if (_layer == 0)
        {
            Instance = this;
        }
    }
}
