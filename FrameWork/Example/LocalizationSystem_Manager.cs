﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class LocalizationSystem_Manager : Cargold.FrameWork.LocalizationSystem_Manager
{
    public static new LocalizationSystem_Manager Instance;

    public override void Init_Func(int _layer)
    {
        base.Init_Func(_layer);

        if (_layer == 0)
        {
            Instance = this;
        }
    }
}
