﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
 
public class Test_Script : MonoBehaviour
{
    public void Init_Func()
    {
        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
	    
    }
 
    public void Deactivate_Func(bool _isInit = false)
    {
	    
    }
}