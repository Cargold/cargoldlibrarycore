﻿using UnityEngine;
using System;

public class TimeSystem_Manager : Cargold.TimeSystem_Manager.TimeSystem_Manager
{
    protected override void Init_Func()
    {
        Cargold.TimeSystem_Manager.TimeSystem_Manager.instance = this;

        UnbiasedTimeManager.UnbiasedTime.Init();
        GameObject _timeObj = UnbiasedTimeManager.UnbiasedTime.Instance.gameObject;
        GameObject.DontDestroyOnLoad(_timeObj);
    }

    public override DateTime Now
    {
        get
        {
            if (UnbiasedTimeManager.UnbiasedTime.Instance.failed == true)
                UnbiasedTimeManager.UnbiasedTime.Instance.Initialize();

            return UnbiasedTimeManager.UnbiasedTime.Instance.dateTime + DateTimeOffset.Now.Offset;
        }
    }
}