﻿public static partial class Debug_C
{
    [System.Flags]
    public enum PrintLogType
    {
        None = 0,

        Common = 1 << 1,

        All = ~None,
    }
}