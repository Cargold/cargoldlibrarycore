﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 0.1.3 ('20.05.02)
// Developed By Cargold
namespace PlayHard.PoolingSystem
{
    public class PoolingSystemManager<PoolingKey>
    {
        private static System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
        private const string PrefixWord = "(PoolerGroup) ";

        protected Transform mainPoolGroupTrf;
        protected Dictionary<PoolingKey, PoolingContainer> poolingContainerDic;

        public PoolingSystemManager(Transform mainPoolGroupTrf)
        {
            this.mainPoolGroupTrf = mainPoolGroupTrf;

            poolingContainerDic = new Dictionary<PoolingKey, PoolingContainer>();
        }
        public PoolingSystemManager(Transform mainPoolGroupTrf, IEqualityComparer<PoolingKey> iEqualityComparer)
        {
            this.mainPoolGroupTrf = mainPoolGroupTrf;

            poolingContainerDic = new Dictionary<PoolingKey, PoolingContainer>(iEqualityComparer);
        }

        public bool TryGeneratePool<ComponentType>(PoolingKey poolingKey, GameObject basePoolerObj, int loadCount, bool isSetParent = true, Transform poolGroupTrf = null) where ComponentType : Component, IPooler
        {
            if (poolingContainerDic.ContainsKey(poolingKey) == false)
            {
                string _poolingName = PoolingSystemManager<PoolingKey>.PrefixWord;

#if UNITY_EDITOR
                stringBuilder.Clear();
                stringBuilder.Append(PrefixWord);
                stringBuilder.Append(poolingKey.ToString());
                _poolingName = stringBuilder.ToString();
#endif

                Transform _poolGroupTrf = null;
                if(poolGroupTrf == null)
                {
                    _poolGroupTrf = new GameObject(_poolingName).transform;
                    _poolGroupTrf.SetParent(mainPoolGroupTrf);
                }
                else
                {
                    _poolGroupTrf = poolGroupTrf;
                }
                
                PoolingContainer<ComponentType> _objectPoolingChild = new PoolingContainer<ComponentType>(_poolGroupTrf, basePoolerObj, loadCount, isSetParent);
                poolingContainerDic.Add(poolingKey, _objectPoolingChild);

                return true;
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("다음 Key는 이미 존재합니다. : " + poolingKey);
#endif

                return false;
            }
        }
        public void GeneratePool<ComponentType>(PoolingKey poolingKey, GameObject basePoolerObj, int loadCount, bool isSetParent = true, Transform poolGroupTrf = null) where ComponentType : Component, IPooler
        {
            this.TryGeneratePool<ComponentType>(poolingKey, basePoolerObj, loadCount, isSetParent, poolGroupTrf);
        }

        public bool TryDestroyPool<ComponentType>(PoolingKey poolingKey) where ComponentType : Component, IPooler
        {
            PoolingContainer _poolingSystem = null;
            if (poolingContainerDic.TryGetValue(poolingKey, out _poolingSystem) == true)
            {
                PoolingContainer<ComponentType> _objectPoolingChild = (PoolingContainer<ComponentType>)_poolingSystem;

                _objectPoolingChild.Dispose();

                poolingContainerDic.Remove(poolingKey);

                return true;
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogWarning("다음 Key는 존재하지 않습니다. : " + poolingKey);
#endif
            }

            return false;
        }
        public void DestroyPool<ComponentType>(PoolingKey poolingKey) where ComponentType : Component, IPooler
        {
            this.TryDestroyPool<ComponentType>(poolingKey);
        }

        public ComponentType Spawn<ComponentType>(PoolingKey poolingKey) where ComponentType : Component, IPooler
        {
            PoolingContainer _poolingSystem = null;
            if (poolingContainerDic.TryGetValue(poolingKey, out _poolingSystem) == true)
            {
                if(_poolingSystem is PoolingContainer<ComponentType>)
                {
                    PoolingContainer<ComponentType> _objectPoolingChild = (PoolingContainer<ComponentType>)_poolingSystem;

                    return _objectPoolingChild.Spawn();
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogError("다음 Key의 캐스팅은 실패했습니다. : " + poolingKey);
#endif

                    return null;
                }
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("다음 Key는 존재하지 않습니다. : " + poolingKey);
#endif

                return null;
            }
        }
        public void Despawn<ComponentType>(PoolingKey poolingKey, ComponentType returnComponent) where ComponentType : Component, IPooler
        {
            PoolingContainer _poolingSystem = null;
            if (poolingContainerDic.TryGetValue(poolingKey, out _poolingSystem) == true)
            {
                if (_poolingSystem is PoolingContainer<ComponentType>)
                {
                    PoolingContainer<ComponentType> _objectPoolingChild = (PoolingContainer<ComponentType>)_poolingSystem;

                    _objectPoolingChild.Despawn(returnComponent);
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogError("다음 Key의 캐스팅은 실패했습니다. : " + poolingKey);
#endif
                }
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("다음 Key는 존재하지 않습니다. : " + poolingKey);
#endif
            }
        }
        public void Despawn<ComponentType>(PoolingKey poolingKey, ComponentType returnComponent, bool isSetParent) where ComponentType : Component, IPooler
        {
            PoolingContainer _poolingSystem = null;
            if (poolingContainerDic.TryGetValue(poolingKey, out _poolingSystem) == true)
            {
                PoolingContainer<ComponentType> _objectPoolingChild = (PoolingContainer<ComponentType>)_poolingSystem;

                _objectPoolingChild.Despawn(returnComponent, isSetParent);
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("다음 Key는 존재하지 않습니다. : " + poolingKey);
#endif
            }
        }

        public int GetLoadedCount<ComponentType>(PoolingKey poolingKey) where ComponentType : Component, IPooler
        {
            PoolingContainer _poolingSystem = null;
            if (poolingContainerDic.TryGetValue(poolingKey, out _poolingSystem) == true)
            {
                PoolingContainer<ComponentType> _poolSystem = (PoolingContainer<ComponentType>)_poolingSystem;

                return _poolSystem.LoadedPoolerNum;
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("다음 Key는 존재하지 않습니다. : " + poolingKey);
#endif

                return -1;
            }
        }
        public bool IsGeneratedPool(PoolingKey poolingKey)
        {
            return this.poolingContainerDic.ContainsKey(poolingKey);
        }
        public bool IsGeneratedPool<ComponentType>(PoolingKey poolingKey) where ComponentType : Component, IPooler
        {
            PoolingContainer _poolingSystem = null;
            if (poolingContainerDic.TryGetValue(poolingKey, out _poolingSystem) == true)
            {
                return _poolingSystem is PoolingContainer<ComponentType>;
            }
            else
            {
                return false;
            }
        }
    }
}