﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayHard.PoolingSystem
{
    public class PoolingContainer { }

    [System.Serializable]
    public class PoolingContainer<PoolerComponent> : PoolingContainer, IDisposable where PoolerComponent : Component, IPooler
    {
        private Transform groupTrf;
        private GameObject basePoolerObj;
        private Queue<PoolerComponent> poolQueue;
        private int loadCount;
        private bool isSetParent;
        private bool isDisposed;
        
        public bool HasObject { get { return 0 < this.poolQueue.Count ? true : false; } }
        public int LoadedPoolerNum { get { return this.poolQueue.Count; } }

        public PoolingContainer(Transform groupTrf, GameObject basePoolerObj, int loadCount, bool isSetParent = true)
        {
            this.groupTrf = groupTrf;
            this.basePoolerObj = basePoolerObj;
            this.poolQueue = new Queue<PoolerComponent>();
            this.loadCount = 0;
            this.isSetParent = isSetParent;
            this.isDisposed = false;


            for (int i = 0; i < loadCount; i++)
            {
                PoolerComponent _poolingComponent = this.GenerateComponent(isSetParent);
                this.poolQueue.Enqueue(_poolingComponent);
            }
        }
        ~PoolingContainer()
        {
            this.Dispose();
        }

        private PoolerComponent GenerateComponent(bool isSetParent = true)
        {
            GameObject _obj = GameObject.Instantiate(basePoolerObj);
#if UNITY_EDITOR
            _obj.name = this.GetPoolerName();
#endif
            if (isSetParent == true && this.groupTrf != null)
                _obj.transform.SetParent(this.groupTrf);

            PoolerComponent _poolingComponent = _obj.GetComponent<PoolerComponent>();
            if (_poolingComponent != null)
            {
                _poolingComponent.InitializedByPoolingSystem();
            }
            else
            {
                Debug.LogError("Null : " + _poolingComponent);
            }

            return _poolingComponent;
        }

        public PoolerComponent Spawn()
        {
            if(0 < this.poolQueue.Count)
            {
                PoolerComponent _pooler = poolQueue.Dequeue();

                return _pooler;
            }
            else
            {
                return this.GenerateComponent(this.isSetParent);
            }
        }
        public void Despawn(PoolerComponent poolingComponent)
        {
            this.Despawn(poolingComponent, this.isSetParent);
        }
        public void Despawn(PoolerComponent poolerComponent, bool isSetParentTrf)
        {
            if (this.poolQueue.Contains(poolerComponent) == false)
            {
                this.poolQueue.Enqueue(poolerComponent);

                if (isSetParentTrf == true)
                    poolerComponent.transform.SetParent(groupTrf);
            }
            else
            {
                Debug.LogWarning("이미 저장되어 있는 Item을 ObjectPooling에 저장하려고 합니다. : " + poolerComponent);
            }
        }

        private string GetPoolerName()
        {
            string _poolerName = this.basePoolerObj.name + "_" + this.loadCount.ToString();

            ++this.loadCount;

            return _poolerName;
        }

        public void Dispose()
        {
            if(this.isDisposed == false)
            {
                this.isDisposed = true;

                while (0 < this.poolQueue.Count)
                {
                    PoolerComponent _pooler = poolQueue.Dequeue();
                    Component.Destroy(_pooler);
                }

                this.groupTrf = null;
                this.basePoolerObj = null;
                this.poolQueue = null;
                this.loadCount = 0;
                this.isSetParent = false;

                System.GC.SuppressFinalize(this);
            }
        }
    }

    public interface IPooler
    {
        void InitializedByPoolingSystem();
    }
}